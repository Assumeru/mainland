Move to T_D?
	Jagga
	amber
	goya
	veig
	ungorth

x	abolitionists
x	alchemy
x	ald sotha
x	almalexia
x	ancestors
x	antecedents of dwemer law
x	argonian
x	arkay
x	ashlander customs
x	attack
x	azura's coast
x	background (remove T_glb_miner)
x	bandits
x	betmeri
x	black marsh
x	bosmer
x	breton
x	camonna tong
x	chimer
x	conjuration
x	current events
x	daedra
x	daedra worship
x	dagoth ur
x	dark brotherhood
x	destruction
x	dibella
x	dreugh
x	dunmer
x	duties
x	dwemer
x	dwemer artifacts
x	dwemer ruins
x	elsweyr
x	empire
x	enchant
x	enchanters
x	enchanting items
x	fighter disciplines
x	fighters guild
x	fines and compensation
x	flee
x	flin
x	fortify potions
x	geography of morrowind
x	geography of vvardenfell
x	get cured
x	good daedra
x	greef
x	greeting 0
x	greeting 2 (remove MG vamp greeting)
x	greeting 4
x	greeting 6
x	greeting 7 (remove guild guide line)
x	guard
x	hammerfell
x	hello
x	high rock
x	hit
x	house dres
x	house hlaalu
x	house indoril
x	house redoran
x	house telvanni
x	idle
x	imperial
x	imperial cult
x	imperial law
x	imperial legion
x	inner sea
x	jobs
x	join the mages guild
x	join the thieves guild
x	khajiit
x	killing and murder
x	latest rumors
x	little advice (check intervention filter)
x	little secret (remove <20 legion/temple, <50 bottom)
x	mages guild (remove savant line)
x	make amends
x	martial arts
x	mazte
x	morag tong
x	morrowind
x	morrowind lore
x	my trade (fix fisherman filter, move riverstrider to T_D, delete hunter)
x	necromancy (change last commoner to pauper)
x	nerevar
x	nerevarine
x	nine divines
x	nord
x	orders
x	outlander
x	red mountain
x	redguard
x	sea of ghosts
x	services
x	shein
x	shrines
x	smuggling
x	someone in particular
x	sotha sil (remove priest lines)
x	soul sickness
x	spear
x	specific place
x	sujamma
x	summerset isle
x	telvanni bug musk
x	temple (remove temple priest line)
x	the empire
x	thief
x	thieves guild (override savant services)
x	tribunal (remove priest lines)
x	tribunal temple (remove priest lines)
x	uriel septim
x	using enchantments
x	valenwood
x	vampires
x	vampirism cure
x	vivec (remove priest lines)
x	war of the first council
x	zenithar
